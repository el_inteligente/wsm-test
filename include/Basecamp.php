<?php

// This is my modification of the Basecamp class
// Original code was taken from https://github.com/prplrs/basecamp-php

class Basecamp {
	private $user;
	private $pass;
	private $acc;
	private $url;
	
	public function __construct($acc, $user, $pass) {
		$this->setAccount($acc);
		$this->setUser($user);
		$this->setPassword($pass);
	}
	
	public function setAccount($acc) {
		$this->acc = $acc;
		$this->setURL($acc);
		return $this;
	}
	
	public function setUser($user) {
		$this->user = $user;
		return $this;
	}
	
	public function setPassword($pass) {
		$this->pass = $pass;
		return $this;
	}
	
	public function setURL($acc = null) {
		$acc = isset($acc) ? $acc : $this->acc;
		$this->url = "https://$acc.basecamphq.com/";
		return $this;
	}
	
	private function extractArray($xml, $property) {
		$array = array();
		
		if (isset($xml->$property)) {
			foreach ($xml->$property as $item) {
				$array[] = $item;
			}
		}
		
		return $array;
	}
	
	public function getMessage($message_id) {
		return $this->request("posts/$message_id.xml");
	}
	
	public function getMessages($project_id, $offset = 0) {
		if ($offset) {
			$n = "?n=$offset";
		} else {
			$n = '';
		}
		
		return $this->extractArray(
			$this->request("projects/$project_id/posts.xml" . $n),
			'post'
		);
	}
	
	public function createMessage($project_id, $title) {
		// there could be more function parameters but they aren't necessary for current task
		$xml = $this->request("projects/$project_id/posts/new.xml");
		$xml->title = $title;
		$xmlText = $xml->asXML();
		$headers = $this->request("projects/$project_id/posts.xml", "<request>$xmlText</request>");
		if (preg_match('#Location:.*?(\d+)\.xml#', $headers, $matches)) {
			return $matches[1];
		}
		return false;
	}
	
	public function createComment($message_id, $body) {
		$xml = $this->request("posts/$message_id/comments/new.xml");
		$xml->body = $body;
		$xmlText = $xml->asXML();
		$headers = $this->request("posts/$message_id/comments.xml", "<request>$xmlText</request>");
		if (preg_match('#Location:.*?(\d+)\.xml#', $headers, $matches)) {
			return $matches[1];
		}
		return false;
	}
	
	public function getProjects() {
		return $this->extractArray(
			$this->request('projects.xml'),
			'project'
		);
	}
	
	public function getProject($id) {
		return $this->request("projects/$id.xml");
	}
	
	public function getTodoItems($list_id) {
		return $this->extractArray(
			$this->request("todo_lists/$list_id/todo_items.xml"),
			'todo-item'
		);
	}
	
	public function getProjectTodoItems($project_id) {
		$lists = $this->getTodoLists($project_id);
		
		$items = array();
		foreach ($lists as $list) {
			$list_items = $this->getTodoItems((string)$list->id);
			$items = array_merge($items, $list_items);
		}
		
		return $items;
	}
	
	public function getTodoLists($project_id = 0) {
		return $this->extractArray(
			$this->request(($project_id ? "projects/$project_id/" : '') . 'todo_lists.xml'),
			'todo-list'
		);
	}
	
	private function isAuthenticated() {
		$result = ($this->acc == null || $this->user == null || $this->pass == null) ? false : true;
		return $result;
	}
	
	private function request($path, $post_xml = '') {
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $this->url . $path);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt(
			$curl,
			CURLOPT_HTTPHEADER,
			array(
				'User-Agent: Anatoly Ivakin (anatoly.ivakin.work@gmail.com)',
				'Content-Type: application/xml',
				'Accept: application/xml'
			)
		);
		if ($post_xml) {
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post_xml);
		} else {
			curl_setopt($curl, CURLOPT_HTTPGET, true);
		}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_USERPWD, $this->user . ":" . $this->pass);
		
		if (curl_errno($curl)) {
			throw new Exception(curl_error($curl));
		}
		
		if (!$this->isAuthenticated()) {
			throw new Exception('Authentication Failed');
		}
		
		$response = curl_exec($curl);
		if (curl_errno($curl) == 0) {
			if ($post_xml) {
				$result = $response;
			} else {
				list(, $body) = explode("\r\n\r\n", $response, 2);
				$result = new SimpleXMLElement($body);
			}
		} else {
			throw new Exception(curl_error($curl));
		}
		
		curl_close($curl);
		
		return $result;
	}

} // end Basecamp Class
