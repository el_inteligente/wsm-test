<?php

function class_autoload($className) {
	$filename = 'include/' . str_replace('\\', '/', $className) . '.php';
	if (file_exists($filename)) {
		include($filename);
		if (class_exists($className)) {
			return true;
		}
	}
	return false;
}

spl_autoload_register('class_autoload');
