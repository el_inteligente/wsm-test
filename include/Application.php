<?php

class Application {
	private static $app; /* @var $app Application */
	
	private $db; /* @var $db Database */
	private $bc; /* @var $bc Basecamp */
	
	private function __construct($config) {
		$this->db = new Database(
			$config['db-host'],
			$config['db-user'],
			$config['db-pass'],
			$config['db-name']
		);
		
		$this->bc = new Basecamp(
			$config['bc-acc'],
			$config['bc-user'],
			$config['bc-pass']
		);
	}
	
	public static function create($config) {
		return self::$app ? self::$app : (self::$app = new Application($config));
	}
	
	public function run() {
		$project_id = filter_input(INPUT_POST, 'project_id', FILTER_VALIDATE_INT);
		if ($project_id) {
			$this->processProject($project_id);
		} else {
			$this->selectProject();
		}
	}
	
	private function selectProject() {
		$projects = $this->bc->getProjects();
		$options = array_map(
			function($p) {
				return array('value' => $p->id, 'text' => $p->name);
			},
			$projects
		);
		self::render('form', $options);
	}
	
	private function processProject($project_id) {
		$bc_items = self::createDictionary(
			$this->bc->getProjectTodoItems($project_id),
			function($i) { return (string)$i->id; }
		);
		
		$db_items = self::createDictionary(
			$this->db->getProjectTodoItems($project_id),
			function($i) { return $i['id']; }
		);
		
		$result = array();
		$keys = array_unique(array_merge(array_keys($bc_items), array_keys($db_items)));
		foreach ($keys as $key) {
			try {
				$bc_has_key = isset($bc_items[$key]);
				$db_has_key = isset($db_items[$key]);
				if ($bc_has_key && $db_has_key) {
					$result[] = $this->determineChanges($bc_items[$key], $db_items[$key]);
				} else if ($bc_has_key) {
					$result[] = $this->itemCreated($project_id, $bc_items[$key]);
				} else if ($db_has_key) {
					$result[] = $this->itemDeleted($db_items[$key]);
				} else {
					throw new Exception('Heavy snowfall occured in Kenya');
				}
			} catch (Exception $ex) {
				// ignore for the current item to let process other items
			}
		}
		
		self::render('project-changes', $result);
	}
	
	private function determineChanges($bc_item, $db_item) {
		$diffs = self::getObjectDifferences($bc_item, new SimpleXMLElement($db_item['xml']));
		$changes = array();
		foreach ($diffs as $key => $diff) {
			extract($diff);
			switch ($status) {
				case 'created':
					$desc = "'$new'";
					break;
				default:
				case 'modified':
					$desc = "'$old' -> '$new'";
					break;
				case 'deleted':
					$desc = "'$old'";
					break;
			}
			$text = "'$key' $status ($desc)";
			$changes[] = $text;
		}
		
		if ($changes) {
			$count = count($changes);
			$result = "Detected $count modification(s) for Todo Item #{$bc_item->id}:\r\n\t";
			$result .= implode(";\r\n\t", $changes) . '.';
			
			$this->db->updateTodoItem($bc_item);
			$this->bc->createComment($db_item['message_id'], $result);
		} else {
			$result = "Todo Item #{$bc_item->id} has no modifications.";
		}
		
		return $result;
	}
	
	private function itemCreated($project_id, $item) {
		$text = "Todo Item #{$item->id}";
		$this->db->insertTodoItem($project_id, $item);
		$message_id = $this->bc->createMessage($project_id, $text);
		if ($message_id) {
			$message = $this->bc->getMessage($message_id);
			$this->db->insertMessage($item->id, $message);
		}
		return $text . ' was created.';
	}
	
	private function itemDeleted($item) {
		$text = "Todo Item #{$item['id']} was deleted.";
		$this->bc->createComment($item['message_id'], $text);
		$this->db->deleteTodoItem($item['id']);
		$this->db->deleteMessage($item['message_id']);
		return $text;
	}
	
	public static function render($view, $data = array()) {
		$path = "views/$view.php";
		if (file_exists($path)) {
			include($path);
		} else {
			throw new Exception("View '$view' does not exist in $path");
		}
	}
	
	private static function createDictionary($values, $extract_key) {
		if ($values && is_callable($extract_key)) {
			$keys = array_map($extract_key, $values);
			return array_combine($keys, $values);
		}
		return array();
	}
	
	private static function getObjectDifferences($new, $old) {
		$new = json_decode(json_encode($new), true);
		$old = json_decode(json_encode($old), true);
		$keys = array_unique(
			array_merge(
				array_keys($new),
				array_keys($old)
			)
		);
		
		$result = array();
		foreach ($keys as $key) {
			if ($key == '@attributes') {
				continue;
			}
			
			$new_has_key = isset($new[$key]);
			$old_has_key = isset($old[$key]);
			if ($new_has_key && $old_has_key) {
				if ($new[$key] != $old[$key]) {
					$result[$key] = array(
						'status' => 'modified',
						'old' => $old[$key],
						'new' => $new[$key]
					);
				}
			} else if ($new_has_key) {
				$result[$key] = array(
					'status' => 'created',
					'new' => $new[$key]
				);
			} else if ($old_has_key) {
				$result[$key] = array(
					'status' => 'deleted',
					'old' => $old[$key]
				);
			} else {
				throw new Exception('The existence of God was proved by scientists');
			}
		}
		
		return $result;
	}
}
