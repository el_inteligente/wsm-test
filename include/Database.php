<?php

class Database {
	/* @var $db mysqli */
	protected $db;
	
	private $host;
	private $name;
	private $user;
	private $pass;
	
	public function __construct($host, $user, $pass, $name) {
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->name = $name;
		
		$this->connect();
	}
	
	private function connect() {
		$this->db = new mysqli(
			$this->host,
			$this->user,
			$this->pass,
			$this->name
		);
		
		if ($this->db->connect_errno > 0) {
			throw new Exception($this->db->connect_error, $this->db->connect_errno);
		}
		
		$tables = $this->queryScalars('SHOW TABLES');
		if (empty($tables)) {
			$structure_sql = file_get_contents('structure.sql');
			$this->db->multi_query($structure_sql);
			while($this->db->more_results()) {
				$this->db->next_result();
				$this->db->store_result(); // actually we don't store anything, just skip returned data
			}
		}
	}
	
	protected function query($query, $fetch = null) {
		/* @var $result mysqli_result */
		$result = $this->db->query($query);
		if ($result) {
			return is_callable($fetch) ? call_user_func($fetch, $result) : $result;
		} else {
			throw new Exception($this->db->error, $this->db->errno);
		}
	}
	
	protected function queryScalars($query) {
		return $this->query($query, array($this, 'fetchScalars'));
	}
	
	protected function queryRows($query) {
		return $this->query($query, array($this, 'fetchRows'));
	}
	
	protected function queryRowsAssoc($query) {
		return $this->query($query, array($this, 'fetchRowsAssoc'));
	}
	
	protected function fetchRows(mysqli_result $result) {
		$array = array();
		
		while ($row = $result->fetch_row()) {
			$array[] = $row;
		}
		
		return $array;
	}
	
	protected function fetchRowsAssoc(mysqli_result $result) {
		$array = array();
		
		while ($row = $result->fetch_assoc()) {
			$array[] = $row;
		}
		
		return $array;
	}
	
	protected function fetchScalars(mysqli_result $result) {
		$array = array();
		
		while ($row = $result->fetch_row()) {
			$array[] = $row[0];
		}
		
		return $array;
	}
	
	public function getProjectTodoItems($project_id) {
		$query = ("
			SELECT
				ti.*,
				m.id AS message_id
			FROM
				todo_items ti
				LEFT JOIN messages m ON ti.id = m.todo_item_id
			WHERE
				project_id = $project_id
		");
		
		return $this->queryRowsAssoc($query);
	}
	
	public function insertTodoItem($project_id, $todo_item) {
		$esc_xml = $this->db->real_escape_string($todo_item->asXML());
		$query = "INSERT INTO todo_items VALUES({$todo_item->id}, $project_id, '{$esc_xml}')";
		
		return $this->query($query);
	}
	
	public function updateTodoItem($todo_item) {
		$esc_xml = $this->db->real_escape_string($todo_item->asXML());
		$query = "UPDATE todo_items SET xml = '$esc_xml' WHERE id = {$todo_item->id}";
		
		return $this->query($query);
	}
	
	public function deleteTodoItem($todo_item_id) {
		$query = "DELETE FROM todo_items WHERE id = $todo_item_id";
		
		return $this->query($query);
	}
	
	public function insertMessage($todo_item_id, $message) {
		$esc_xml = $this->db->real_escape_string($message->asXML());
		$query = "INSERT INTO messages VALUES({$message->id}, $todo_item_id, '{$esc_xml}')";
		
		return $this->query($query);
	}
	
	public function deleteMessage($message_id) {
		$query = "DELETE FROM messages WHERE id = $message_id";
		
		return $this->query($query);
	}
}
