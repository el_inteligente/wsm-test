<?php

require_once 'config.php';
require_once 'include/autoload.php';

try {
	Application::create($config)->run();
} catch (Exception $ex) {
	Application::render('error', $ex);
}
