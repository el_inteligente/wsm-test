CREATE TABLE `todo_items` (
	`id` INT UNSIGNED NOT NULL,
	`project_id` INT UNSIGNED NOT NULL,
	`xml` TEXT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `messages` (
	`id` INT UNSIGNED NOT NULL,
	`todo_item_id` INT UNSIGNED NOT NULL,
	`xml` TEXT NULL,
	PRIMARY KEY (`id`)
);
